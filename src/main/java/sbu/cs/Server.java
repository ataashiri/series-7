package sbu.cs;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server
{
    final static int PORT = 8888;

    public static void main(String[] args) throws IOException
    {
        String directory = args[0];
        ServerSocket serverSocket = new ServerSocket(PORT);
        Socket socket = serverSocket.accept();
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        FileOutputStream fileOutputStream = new FileOutputStream(directory + "/" + readFileName(dataInputStream));
        writeFile(fileOutputStream,readByteData(dataInputStream,readByteSize(dataInputStream)));
        dataInputStream.close();
        fileOutputStream.flush();
        fileOutputStream.close();
    }
    private static byte[] readByteData(DataInputStream dataInputStream,int byteSize) throws IOException
    {
        byte[] byteData = new byte[byteSize];
        dataInputStream.readFully(byteData);
        return byteData;
    }
    private static void writeFile(FileOutputStream fileOutputStream,byte[] byteData) throws IOException
    {
        fileOutputStream.write(byteData);
    }

    private static String readFileName(DataInputStream dataInputStream) throws IOException
    {
        return dataInputStream.readUTF();
    }

    private static int readByteSize(DataInputStream dataInputStream) throws IOException
    {
        return dataInputStream.readInt();
    }
}