package sbu.cs;

import java.io.*;
import java.net.Socket;

public class Client
{
    final static int PORT = 8888;

    public static void main(String[] args) throws IOException
    {
        String filePath = args[0];
        Socket clientSocket = new Socket("localhost",PORT);
        DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
        FileInputStream fileInputStream = new FileInputStream(filePath);
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        writeName(dataOutputStream,filePath);
        writeSize(dataOutputStream,findSize(dataInputStream));
        writeByteData(dataOutputStream,readFile(dataInputStream,findSize(dataInputStream)));
        dataOutputStream.flush();
        dataOutputStream.close();
        clientSocket.close();
    }

    private static int findSize(DataInputStream dataInputStream) throws IOException
    {
        return dataInputStream.available();
    }

    private static void writeName(DataOutputStream dataOutputStream,String filePath) throws IOException
    {
        dataOutputStream.writeUTF(filePath);
        dataOutputStream.flush();
    }

    private static byte[] readFile(DataInputStream dataInputStream,int byteSize) throws IOException
    {
        byte[] byteData = new byte[byteSize];
        dataInputStream.readFully(byteData);
        return byteData;
    }

    private static void writeSize(DataOutputStream dataOutputStream,int byteSize) throws IOException
    {
        dataOutputStream.writeInt(byteSize);
    }

    private static void writeByteData(DataOutputStream dataOutputStream,byte[] byteData) throws IOException
    {
        dataOutputStream.write(byteData);
    }
}